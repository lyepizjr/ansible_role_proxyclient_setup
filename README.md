ansible_role_net_proxy
=================

Role to configure a linux host to use a web proxt

Requirements
------------

none at this time


Notible Features of this role

* Setup profile to force users to use a proxy

Prerequisites

This role assumes you have a proxy server configuration and running in your environment


Mandatory Variables

* proxy_server_ip
* proxy_server_port

Role Variables
--------------

```yaml

# string - set ip address of the proxy server
proxy_server_ip:

# int - set port of the proxy server
proxy_server_port

```
Example Playbook
----------------

```yaml
- hosts: client
  become: yes
  user: ansible
  vars: 
    proxy_server_ip: 192.168.1.145
    proxy_server_port: 8080
  roles:
    - ansible_role_net_proxy
```

License
-------

BSD


Author Information
-------------------

RRC CSG Dev Team

